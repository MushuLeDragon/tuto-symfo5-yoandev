# Tuto Symfony 5 Yonadev

Tuto Symfony 5 par Yoandev ([Playlist here](https://youtube.com/playlist?list=PLxEJ5uJLOPDys4MgOz78lci7e7g5GoolQ)) sur la mise en place d'un blog de peintures.

## Requirements

- PHP 7.4
- Symfony 4.26
- Symfony CLI
- Composer
- Docker
- Docker-compose
- NodeJS
- NPM

```shell
# Remove Git permissions tracking
git config --global core.fileMode false

# Packages
cd ~
curl -sL https://deb.nodesource.com/setup_14.x | bash -
sudo apt install nodejs npm php7.4-cli php7.4-curl php7.4-xml php-codesniffer unzip

# Install Composer
curl -sSk https://getcomposer.org/installer | php -- --disable-tls && mv composer.phar /usr/local/bin/composer

# Install Symfony
curl -sS https://get.symfony.com/cli/installer | bash && mv /root/.symfony/bin/symfony /usr/local/bin/symfony && echo 'alias sc="symfony console"' >> ~/.bashrc

symfony check:requirements
```

## Install the project

```shell
symfony new my_project --full

composer install
# composer install --ignore-platform-reqs
# composer require symfony/apache-pack # Vérifier si on en a besoin lors du lancement du site Symfony

# Create Docker database
symfony console make:docker:database

# Run the Symfony server
symfony serve -d

# Install Webpack encore
composer require symfony/webpack-encore-bundle
npm install

npm install sass-loader@^12.0.0 sass --save-dev
npm install postcss-loader autoprefixer --dev
npm install bootstrap @popperjs/core
echo "module.exports = {
  plugin: {
    autoprefixer: {},
  },
};" > postcss.config.js

echo "import './styles/app.scss';
// You can specify which plugins you nedd
import { Tooltip, Toast, Popover } from 'bootstrap';" >> ./assets/app.js
echo '@import "~bootstrap/scss/bootstrap";' > ./assets/styles/app.scss

npm run build
```

## Run the project

```bash
composer install
npm install
npm run build
docker-compose up -d
symfony serve -d
```

# Tests

```shell
# CodeSniffer
sudo apt install php-codesniffer
phpcs -v --standard=PSR12 --ignore=./src/Kernel.php ./src

# PHP Stan
composer require --dev phpstan/phpstan
vendor/bin/phpstan analyse ./src tests

# Twig lint
composer global require "asm89/twig-lint" "@stable"
# twig-lint lint ./templates

# Create Symfony Unit Tests
symfony console make:unit-test

# Run Tests
php bin/phpunit --testdox
```